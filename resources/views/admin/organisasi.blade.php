@extends('admin/master')

@section('content')
  <style media="screen">
    .form-group {
      margin-bottom: 0
    }
  </style>
  <section class="py-5">
    <div class="row">
      <div class="col-lg-12 mb-8">
        <div class="card">
          <div class="card-header">
            <h6 class="text-uppercase mb-0">Organisasi Keagamaan</h6>
          </div>
          <div class="card-body">
            <div class="form-group row">
              <label class="col-md-2 form-control-label">Kecamatan</label>
              <div class="col-md-5 select mb-3">
                <select id="lunchBegins" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Please select a lunch ...">
                  <option>Hot Dog, Fries and a Soda</option>
                  <option>Burger, Shake and a Smile</option>
                  <option>Sugar, Spice and all things nice</option>
                  <option>Baby Back Ribs</option>
                  <option>A really really long option made to illustrate an issue with the live search in an inline form</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 form-control-label">Tahun</label>
              <div class="col-md-5 select mb-3">
                <select id="lunchBegins" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Please select a lunch ...">
                  <option>Hot Dog, Fries and a Soda</option>
                  <option>Burger, Shake and a Smile</option>
                  <option>Sugar, Spice and all things nice</option>
                  <option>Baby Back Ribs</option>
                  <option>A really really long option made to illustrate an issue with the live search in an inline form</option>
                </select>
              </div>
            </div>
            <table class="table card-text">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nama Kelurahan</th>
                  <th>Jumlah Penduduk</th>
                  <th>Tahun</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>@mdo</td>
                  <td>hapus edit</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>@fat</td>
                  <td>hapus edit</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Larry</td>
                  <td>the Bird</td>
                  <td>@twitter</td>
                  <td>hapus edit</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </section>
@endsection
