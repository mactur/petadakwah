<?php

use Illuminate\Database\Seeder;

class Data_keagamaanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          \App\Data_keagamaan::create([
                  'nama_kelurahan'	=> str_random(20),
                  'jumlah_penduduk' => str_random(11),
                  'islam'           => str_random(11),
                  'kristen_protestan' => str_random(11),
                  'kristen_katolik' => str_random(11),
                  'hindu'           => str_random(11),
                  'budha'           => str_random(11),
                  'konghucu'        => str_random(11),
                  'jml_masjid'      => str_random(2),
                  'pesantren'       => str_random(2),
                  'jml_dai' => str_random(11),
                  'gereja' => str_random(2),
                  'pura' => str_random(2),
                  'kelenteng' => str_random(2),
                  'keterangan' => str_random(11),
                  'tahun' => str_random(11),
                  'id_kecamatan' => str_random(11),
                  // 'alamat'	=> str_random(20),
                  // 'email'	=> str_random(10) . '@gmail.com',
                  // 'telp'    => str_random(11),
                  // 'id_kab'  => str_random(11),
                  // 'latitude'=> str_random(11),
                  // 'longitude' => str_random(11)
            ]);
            factory(\App\Data_keagamaan::class, 8)->create();
    }
}
