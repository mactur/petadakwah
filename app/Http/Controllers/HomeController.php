<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organisasi_keagamaan as organisasi;
use App\Problem_keagamaan as problem;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('layouts/master');
    }

    public function admin() {
      return view('admin/home');
    }
}
