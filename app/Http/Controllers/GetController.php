<?php

namespace App\Http\Controllers;

use App\Organisasi_keagamaan as organisasi;
use App\Problem_keagamaan as problem;
use Illuminate\Http\Request;

class GetController extends Controller {

    public function datas($get="") {
        $organisasi = organisasi::all();
        $problem = problem::all();
        switch ($get) {
            case 'organisasi':
                $data = array("type" => "organisasi", "message" => $organisasi);
                break;
            case 'problem':
                $data = array("type" => "problem", "message" => $problem);
                break;
            default:
                $data = array("type" => "sst", "message" => "hey heyy, tayooo");
                break;
        }

        echo json_encode($data);;
    }
}
