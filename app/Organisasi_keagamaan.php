<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisasi_keagamaan extends Model
{
  protected $table = 'organisasi_keagamaan';
  protected $guarded = [];
  protected $dates = ['deleted_at'];
}
